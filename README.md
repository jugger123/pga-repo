# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Project No 32.
* This repositiory contains api for PayUMoney Payment gateway Integartion.
* Version 1.0
* Languages : Html, Php.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Start with the PayUMoney_form.php
* Fill all the mandatory fields in the form (These are fields that must be sent in the url).
* Enter the success URI and failure URI. 
* These URI are those pages that are to be returned to on successful and unsuccessful transactions.
* Click submit, It redirects to the payment gateway host where you can enter card details.

* No Database configuration required.
* Deploy the code in local host and you are ready to go.

### Test Card Details ###

* Card 1
* Card Type: Visa
* Card Name: Test
* Card Number: 4012001037141112
* Expiry Date : 05/20
* CVV : 123

* Card 2
* Card Type: Master
* Card Name: Test
* Card Number: 5123456789012346
* Expiry Date : 05/20
* CVV : 123

### Who do I talk to? ###

* For any clarification on the implementation, Please contact Areeb (areeb.uddin@chidhagni.com)